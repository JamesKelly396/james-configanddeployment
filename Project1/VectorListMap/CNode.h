#ifndef _CNode_HG_
#define _CNode_HG_

#include "CPerson.h"

class CNode
{
public:
	CNode();
	~CNode(); 
	CPerson peopleData;		// Replace with CPerson
	CNode* pNext;
	void printDebugInfo(void);
};

#endif