#ifndef _CVector_HG_
#define _CVector_HG_

#include "CPerson.h"

class CVector
{
public:
	CVector();
	CVector(int maxSize);
	CVector(const CVector &v);
	~CVector();
	void pushback(CPerson data);

	unsigned int size();
	CPerson operator[](int i);
	bool at(int i, CPerson &foundPerson);

	// Would not be in a map, but bear with me...
	void setMaxSize(unsigned int maxSize);
private:
	void alloc_new();
	unsigned int m_maxSize;
	unsigned int m_size;
	CPerson* m_array;

};

#endif