#include "CMap.h"

CMap::CMap()
{

}

CMap::~CMap()
{

}

unsigned int CMap::m_calcHash(unsigned int lookupIndex)
{
	unsigned int hashValue = lookupIndex % this->m_maxSize;
	return hashValue;
}

unsigned int CMap::m_calcHashString(std::string indexString)
{
	unsigned int totalHashTemp = 0;
	for (int charIndex = 0; charIndex != indexString.size(); charIndex++)
	{
		char curChar = indexString[charIndex];
		totalHashTemp += static_cast<unsigned int>(curChar);
	}
	unsigned int hashVal = this->m_calcHash(totalHashTemp);
	return hashVal;
}

void CMap::storeString(std::string index_, CPerson data)
{
	unsigned int actualIndex = this->m_calcHashString(index_);
	CData temp;
	temp.indexString = index_;
	temp.theData = data;
	this->m_vecData[actualIndex] = temp;
}

bool CMap::lookupPerson(std::string index_, CPerson &foundPerson)
{
	unsigned int actualIndex = this->m_calcHashString(index_);

	CData result = this->m_vecData[actualIndex];
	if (result.theData.firstName == "")
	{	// Nothing there
		return false;
	}
	foundPerson = result.theData;
	return true;
}


void CMap::store(unsigned int index_, CPerson data)
{
	// Calculate actual index (used inside) from index_
	// Use the ... wait for it.... wait for it...
	// MODULUS OPERATOR    **FOR THE WIN** to calculate this

	//unsigned int actualIndex = index_ % this->m_maxSize;
	unsigned int actualIndex = this->m_calcHash(index_);

	CData tempData;
	tempData.index = index_;
	tempData.theData = data;

	this->m_vecData[actualIndex] = tempData;

	return;
}

bool CMap::lookup(unsigned int index_, CPerson &foundPerson)
{
	unsigned int actualIndex = this->m_calcHash(index_);

	// Is there anything in that location?
	// 
	if (this->m_vecData[actualIndex].theData.firstName == "")
	{
		return false;	// Yeah, I know, it's redundant redundant
	}
	foundPerson = this->m_vecData[actualIndex].theData;
	return true;
}


void CMap::setMaxSize(unsigned int maxSize)
{
	this->m_maxSize = maxSize;
	// Make sure the vector is actually this size
	if (this->m_vecData.size() < this->m_maxSize)
	{
		// TODO: Copy the old data to the new size (for the student, tonight...)
		CData empty;
		empty.index = 0;	// 
		CPerson tempPerson;
		tempPerson.firstName = "";
		tempPerson.lastName = "";
		tempPerson.ID = 0;
		empty.theData = tempPerson;
		this->m_vecData.resize(this->m_maxSize, empty);
		// Now the vector is FOR SURE that size
	}
	return;
}

unsigned int CMap::size(){
	return this->m_maxSize;
}

