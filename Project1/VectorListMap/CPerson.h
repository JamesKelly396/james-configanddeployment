#ifndef _CPerson_HG_
#define _CPerson_HG_

#include <string>

class CPerson 
{
public:
	/// Constructor
	CPerson();
	/// Destructor
	virtual ~CPerson();

	std::string firstName;
	std::string lastName;
	int ID;
};

#endif