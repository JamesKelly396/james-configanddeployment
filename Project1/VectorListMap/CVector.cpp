#include "CVector.h"

CVector::CVector()
{
	m_maxSize = 20;
	m_array = new CPerson[m_maxSize];
	m_size = 0;
}

CVector::CVector(int maxSize)
{
	m_maxSize = maxSize;
	m_array = new CPerson[m_maxSize];
	m_size = 0;
}

CVector::CVector(const CVector &v)
{

}

CVector::~CVector()
{
	delete[] m_array;
}


void CVector::pushback(CPerson data)
{
	if (m_size + 1 > m_maxSize)
		alloc_new();
	m_array[m_size] = data;
	m_size++;

	return;
}

void CVector::alloc_new()
{
	m_maxSize = m_size * 2;
	CPerson *tmpArray = new CPerson[m_maxSize];
	for (int i = 0; i < m_size; i++)
		tmpArray[i] = m_array[i];
	delete[] m_array;
	m_array = tmpArray;
}

CPerson CVector::operator[](int i)
{
	return m_array[i];
}

bool CVector::at(int i, CPerson &foundPerson)
{
	if (i < m_size){
		foundPerson = m_array[i];
		return true;
	}
	return false;
}

unsigned int CVector::size(){
	return this->m_size;
}