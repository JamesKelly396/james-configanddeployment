#ifndef _CMap_HG_
#define _CMap_HG_

#include <string>
#include <vector>
#include <iostream>

#include "CPerson.h"

class CData
{
public:
	unsigned int index;				// For storage by int index
	std::string indexString;		// For storage by string index
	CPerson theData;
};

class CMap
{
public:
	CMap();
	~CMap();
	void store(unsigned int index_, CPerson data);
	bool lookup(unsigned int index_, CPerson &foundPerson);

	void storeString(std::string index_, CPerson data);
	bool lookupPerson(std::string index_, CPerson &foundPerson);

	unsigned int size();

	// Would not be in a map, but bear with me...
	void setMaxSize(unsigned int maxSize);
private:
	unsigned int m_calcHash(unsigned int lookupIndex);
	// For strings...
	unsigned int m_calcHashString(std::string indexString);
	std::vector< CData > m_vecData;
	//std::vector< std::vector<CMyData> > m_vecData;
	unsigned int m_maxSize;
	unsigned int m_size;
};

#endif