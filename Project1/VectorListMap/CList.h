#ifndef _CList_HG_
#define _CList_HG_

#include "CNode.h" 

class CList
{
public:
	CList();
	CNode* pHeadNode;
	// Note that we have to determine where 
	//	we want to insert (or we could make 'begin' and 'end' locations)
	void Insert(int indexWhere, CPerson data);
	void InsertAtCurrent(CPerson data);
	void MoveNext(void);
	void MoveToStart(void);
	CNode* getCurrentNode(void);
	CNode* m_CurrentNode;
	unsigned int size();
private:
	unsigned int m_size;
};

CList::CList()
{
	this->pHeadNode = 0;
	this->m_CurrentNode = 0;
	this->m_size = 0;
}

void CList::MoveToStart(void)
{
	// Make the 'current' node the same as the 'head' (or start)
	this->m_CurrentNode = this->pHeadNode;
}

CNode* CList::getCurrentNode(void)
{
	// Return the current node we're pointing to (even if it's nothing)
	return this->m_CurrentNode;
}

// Move the "next" node down the list of nodes...
void CList::MoveNext(void)
{
	// Look at the current node.
	// Change it to the same as the "next" node
	this->m_CurrentNode = this->m_CurrentNode->pNext;
}

void CList::InsertAtCurrent(CPerson data)
{
	m_size++;
	// Are there any nodes? 
	if (this->pHeadNode == 0)
	{
		this->m_CurrentNode = new CNode();
		this->m_CurrentNode->peopleData = data;
		this->pHeadNode = this->m_CurrentNode;
	}
	else
	{
		// Insert a new node on the "next" node of the "current" node
		// Save the "next" node
		CNode* pOldNextNode = this->m_CurrentNode->pNext;

		// Create  new node at the current node...
		this->m_CurrentNode->pNext = new CNode();
		this->m_CurrentNode->pNext->peopleData = data;

		// Point this new node's "next" value to the old "next"
		// This effectively "inserts" in between nodes
		this->m_CurrentNode->pNext->pNext = pOldNextNode;

		this->MoveNext();
	}
}

unsigned int CList::size(){
	return this->m_size;
}

#endif