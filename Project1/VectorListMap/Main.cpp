#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <algorithm>

#include "CVector.h"
#include "CList.h"
#include "CMap.h"

#include "SuperMath.h"
#include "gtest/gtest.h"


MathFuncs::MyMathFuncs mySuperMath;

std::vector<CPerson> g_vec_file;

bool MakeFile(std::string lastfileLocation, std::string malefileLocation);
bool ReadFile(std::string lastfileLocation);
void StoreInStdVector(std::vector<CPerson> &vec_persons);
void StoreInStdList(std::list<CPerson> &list_persons);
void makeCustomVector(CVector &myVector);
int makeCustomList(CList &myList);
int makeCustomMap(CMap &myMap);

bool sortByName(const CPerson &lhs, const CPerson &rhs);

// Whitebox Tests #1: Tests by seeing if a file is successfully opened
TEST(FileReadTest, True) {

	EXPECT_TRUE(ReadFile("USCen/US_Names2.txt"));
}

TEST(FileReadTest, False) {

	EXPECT_FALSE(ReadFile("USCen/US_.txt"));
}


// Whitebox Tests #2: test with a void function 
CVector myVectorTest;
TEST(CustomMakeVectorTest, Zero) {
	makeCustomVector(myVectorTest);
}


// Whitebox Tests #3: Tests custom Vector class to see if a value can be successfully stored
CPerson myPersonTest;
TEST(CustomVectorPushBackTest, Zero) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myVectorTest.pushback(myPersonTest);
}

// Whitebox Tests #4: Determines if the first value come before the second value alphabetically (sorted by last names first)
CPerson myPersonTest2;
TEST(NameSortTest, True) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myPersonTest2.firstName = "Kames";
	myPersonTest2.lastName = "Lelly";
	myPersonTest2.ID = 1;
	EXPECT_TRUE(sortByName(myPersonTest, myPersonTest2));
}


// Whitebox Tests #5: Stores value in custom map and detrmines if the value can be referrenced later
CMap myMapTest;
TEST(MapLookupTest, True) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myMapTest.setMaxSize(1);
	myMapTest.store(myPersonTest.ID, myPersonTest);
	CPerson myPersonReturnValue;
	EXPECT_TRUE(myMapTest.lookup(0, myPersonReturnValue));
}


// Whitebox Tests #6: Stores value in custom map and then returns how many entires are inside the map
TEST(MapSizeTest, One) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myMapTest.setMaxSize(1);
	myMapTest.store(myPersonTest.ID, myPersonTest);
	EXPECT_EQ(1, myMapTest.size());
}


// Whitebox Tests #7: Stores value in custom vector and then detrmines if the value can be referrenced later
TEST(CustomVectorAtTest, True) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myVectorTest.pushback(myPersonTest);
	CPerson myPersonReturnValue;
	EXPECT_TRUE(myVectorTest.at(0, myPersonReturnValue));
}

// Whitebox Tests #8: Stores value in custom vector and then returns how many entires the vector contains
CVector myVectorTest2;
TEST(CustomVectorSizeTest, One) {
	myPersonTest.firstName = "James";
	myPersonTest.lastName = "Kelly";
	myPersonTest.ID = 0;
	myVectorTest2.pushback(myPersonTest);
	EXPECT_EQ(1, myVectorTest2.size());
}

//Blackbox testing, linked to the BlackboxTesting.dll and BlackboxTesting.lib 
//Blackbox Tests #1: Returns the Dot Product
TEST(DotProductTest, 3){
	EXPECT_FLOAT_EQ(3, mySuperMath.DotProduct(1,1,1,1,1,1));
}

//Blackbox Tests #1: Returns the Distance between two points
TEST(DistanceTest, OnePointSeven){
	EXPECT_FLOAT_EQ(1.7320508, mySuperMath.Distance(1, 1, 1, 2, 2, 2));
}

bool ReadFile(std::string lastfileLocation)
{
	std::cout << "Reading from file " << lastfileLocation << std::endl;
	std::ifstream NameFile((lastfileLocation).c_str());

	//Check if file is open
	if (!NameFile.is_open())
		return false;

	std::string lastNameTempString;
	std::string firstNameTempString;
	std::string tempString;

	//Read in the file
	while (!NameFile.eof())
	{
		CPerson tempPerson;
		NameFile >> firstNameTempString;
		tempPerson.firstName = firstNameTempString;

		NameFile >> lastNameTempString;
		tempPerson.lastName = lastNameTempString;

		NameFile >> tempString;
		tempPerson.ID = atoi(tempString.c_str());

		g_vec_file.push_back(tempPerson);
		//std::cout << tempPerson.firstName << " " << tempPerson.lastName << " " << tempPerson.ID << std::endl;
	}

	NameFile.close();
	return true;
}

void StoreInStdVector(std::vector<CPerson> &vec_persons){
	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		vec_persons.push_back(g_vec_file[index]);
	}
	std::vector<CPerson>::iterator it;
	for (it = vec_persons.begin(); it != vec_persons.end(); ++it){
		//std::cout << it->firstName << " " << std::endl;
	}
	it--;
	//std::cout << it->firstName << " " << it->lastName << " " << it->ID << std::endl;
}

void StoreInStdList(std::list<CPerson> &list_persons){
	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		list_persons.push_back(g_vec_file[index]);
	}
	std::list<CPerson>::iterator it;
	for (it = list_persons.begin(); it != list_persons.end(); ++it){
		//std::cout << it->firstName << " " << std::endl;
	}
	it--;
	std::cout << it->firstName << " " << it->lastName << " " << it->ID << std::endl;
}

void StoreInStdMap(std::map<int, CPerson> &map_persons){
	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		map_persons[g_vec_file[index].ID] = g_vec_file[index];
	}
	std::map<int, CPerson>::iterator it;
	for (it = map_persons.begin(); it != map_persons.end(); ++it){
		//std::cout << it->second.firstName << " " << std::endl;
	}
	it = map_persons.begin();
	std::cout << it->second.firstName << " " << it->second.lastName << " " << it->second.ID << std::endl;
}


void makeCustomVector(CVector &myVector)
{
	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		myVector.pushback(g_vec_file[index]);
	}
	unsigned int index = 0;
	for (index = 0; index < myVector.size(); ++index){
		//std::cout << myVector[index].firstName << " " << myVector[index].lastName << " " << myVector[index].ID << std::endl;
	}
	index--;
	std::cout << myVector[index].firstName << " " << myVector[index].lastName << " " << myVector[index].ID << std::endl;
} 

int makeCustomList(CList &myList)
{
	//// Move to the start (in the STL, this is using an iterator object)
	myList.MoveToStart();

	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		myList.InsertAtCurrent(g_vec_file[index]);
	}

	myList.MoveToStart();
	for (unsigned int count = 0; count < myList.size() - 1; count++)
	{
		//myList.getCurrentNode()->printDebugInfo();
		myList.MoveNext();
	}
	myList.getCurrentNode()->printDebugInfo();
	return 0;
}

int makeCustomMap(CMap &myMap)
{
	myMap.setMaxSize(g_vec_file.size());

	for (unsigned int index = 0; index < g_vec_file.size(); index++)
	{
		myMap.store(g_vec_file[index].ID, g_vec_file[index]);
	}
	unsigned int index = 0;
	for (index = 0; index < myMap.size(); ++index){
		/*CPerson tempPerson;
		if (myMap.lookup(index, tempPerson))
		std::cout << tempPerson.firstName << " " << tempPerson.lastName << " " << tempPerson.ID << std::endl;*/
	}
	CPerson tempPerson;
	if (myMap.lookup(index, tempPerson))
		std::cout << tempPerson.firstName << " " << tempPerson.lastName << " " << tempPerson.ID << std::endl;
	return 0;
}



void SearchStdVectorByName(std::string name, bool bfirstName, std::vector<CPerson> vec_persons)
{
	std::vector<CPerson>::iterator it;
	for (it = vec_persons.begin(); it != vec_persons.end(); ++it){
		if (it->firstName == name && bfirstName == true)
			std::cout << it->firstName << " " << it->lastName << " " << it->ID << std::endl;
		else if (it->lastName == name && bfirstName == false)
			std::cout << it->firstName << " " << it->lastName << " " << it->ID << std::endl;
	}
}


bool sortByName(const CPerson &lhs, const CPerson &rhs) { 
	std::string leftName = lhs.lastName + " " + lhs.firstName;
	std::string rightName = rhs.lastName + " " + rhs.firstName;
	return leftName < rightName;
}

auto cmp = [](std::pair<int, CPerson> const & lhs, std::pair<int, CPerson> const & rhs)
{
	std::string leftName = lhs.second.lastName + " " + lhs.second.firstName;
	std::string rightName = rhs.second.lastName + " " + rhs.second.firstName;
	return  leftName < rightName;
};

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	//MakeFile("USCen/US_LastNames.txt", "USCen/onlymalenames.txt");
	//if(!ReadFile("USCen/US_Names2.txt"))
	//	return 0;

	//std::cout << "Loading into std vector" << std::endl;
	//std::vector<CPerson> vec_persons;
	//StoreInStdVector(vec_persons);
	//
	//std::cout << "Loading into std list" << std::endl;
	//std::list<CPerson> list_persons;
	//StoreInStdList(list_persons);

	//std::cout << "Loading into std map" << std::endl;
	//std::map<int, CPerson> map_persons;
	//StoreInStdMap(map_persons);

	//std::cout << "Loading into custom vector" << std::endl;
	//CVector myVector;
	//makeCustomVector(myVector);

	//std::cout << "Loading into custom list" << std::endl;
	//CList myList;
	//makeCustomList(myList);

	//std::cout << "Loading into custom map" << std::endl;
	//CMap myMap;
	//makeCustomMap(myMap);

	//std::cout << "Searching the first name 'ALEX' into std vector" << std::endl;
	//SearchStdVectorByName("ALEX", true, vec_persons);
	//std::cout << "Searching the last name 'STRANO' into std vector" << std::endl;
	//SearchStdVectorByName("STRANO", false, vec_persons);
	//std::cout << "Sorting the names alphabetically for std vector" << std::endl;
	//std::sort(vec_persons.begin(), vec_persons.end(), sortByName);
	//for (CPerson &n : vec_persons)
	//	std::cout << n.lastName << " " << n.firstName << " " << n.ID << std::endl;

	//std::cout << "Sorting the names alphabetically for std list" << std::endl;
	//list_persons.sort(sortByName);
	//for (CPerson &n : list_persons)
	//	std::cout << n.lastName << " " << n.firstName << " " << n.ID << std::endl;

	//std::cout << "Sorting the names alphabetically for std map" << std::endl;
	//std::vector<std::pair<std::string, std::string>> items;
	//std::map<int, CPerson>::iterator it;
	//for (it = map_persons.begin(); it != map_persons.end(); ++it){
	//	std::pair<std::string, std::string> item;
	//	item.first = it->second.lastName;
	//	item.second = it->second.firstName;
	//	items.push_back(item);
	//}
	//std::sort(items.begin(), items.end());
	//std::vector<std::pair<std::string, std::string>>::iterator it2;
	//for (it2 = items.begin(); it2 != items.end(); ++it2)
	//	std::cout << it2->first << " " << it2->second << std::endl;

	

	return 0;
}




bool MakeFile(std::string lastfileLocation, std::string malefileLocation)
{
	std::ifstream LastNameFile((lastfileLocation).c_str());
	if (!LastNameFile.is_open()) //Check if file is open
		return false;
	
	std::string tempString;
	std::string prevLastNameString;
	std::string firstNameTempString;

	bool bRepeatLastName = false;
	std::vector<CPerson *> vec_file;
	//Read in the file
	while (!LastNameFile.eof())
	{
		std::ifstream maleFirstNameFile((malefileLocation).c_str());
		if (!maleFirstNameFile.is_open())
			return false;
		while (maleFirstNameFile >> firstNameTempString && !LastNameFile.eof())
		{
			CPerson *tempPerson = new CPerson();
			//Last name repeater to make sure there are multiple copies of last names
			LastNameFile >> tempString;
			if (bRepeatLastName){
				tempPerson->lastName = prevLastNameString;
			}
			else{
				tempPerson->lastName = tempString;
			}
			prevLastNameString = tempString;
			bRepeatLastName = !bRepeatLastName;

			//maleFirstNameFile >> firstNameTempString;
			tempPerson->firstName = firstNameTempString;

			LastNameFile >> tempString; //junk
			LastNameFile >> tempString; //junk

			LastNameFile >> tempString;
			tempPerson->ID = atoi(tempString.c_str());
			vec_file.push_back(tempPerson);
		}
		maleFirstNameFile.close();
		//std::cout << tempPerson->firstName << " " << tempPerson->lastName << " " << tempPerson->ID << std::endl;
	}
	LastNameFile.close();


	std::ofstream myFile;
	myFile.open("USCen/US_Names2.txt");

	for (unsigned index = 0; index < vec_file.size(); index++)
	{
		myFile << vec_file[index]->firstName << " " << vec_file[index]->lastName << " " << vec_file[index]->ID << std::endl;
	}

	myFile.close();
	return true;
}