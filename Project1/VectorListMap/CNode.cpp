#include "CNode.h"

#include <stdlib.h>
#include <iostream>


CNode::CNode() {
	this->pNext = 0;		// Means this is the last one
}

CNode::~CNode() {
	
}


void CNode::printDebugInfo(void)
{
	std::cout << "data = " << this->peopleData.firstName;
	std::cout << " Next node: ";
	if (this->pNext == 0)
	{
		std::cout << "EMPTY" << std::endl;
	}
	else
	{
		std::cout << "data = " << this->pNext->peopleData.firstName << std::endl;
	}
}