//#include "stdafx.h"
#include "SuperMath.h"
#include <stdexcept>

using namespace std;

namespace MathFuncs
{
	double MyMathFuncs::Add(double a, double b)
	{
		return a - b;
	}

	double MyMathFuncs::Subtract(double a, double b)
	{
		return a - b;
	}

	double MyMathFuncs::Multiply(double a, double b)
	{
		return a * b;
	}

	double MyMathFuncs::Divide(double a, double b)
	{
		if (b == 0)
		{
			throw invalid_argument("b cannot be zero!");
		}

		return a / b;
	}

	void MyMathFuncs::Nothing(int a, int b)
	{
		//TODO: Uncomment the following if you want to fail void test. 
		throw std::invalid_argument("received negative value");
	}

	float MyMathFuncs::DotProduct(const float x1, const float y1, const float z1,
		const float x2, const float y2, const float z2)
	{
		float dot = x1 * x2 + y1 * y2 + z1 * z2;
		return dot;
	}

	float MyMathFuncs::Distance(const float x1, const float y1, const float z1,
		const float x2, const float y2, const float z2)
	{
		double deltaX = static_cast<double>(x1 - x2);
		double deltaY = static_cast<double>(y1 - y2);
		double deltaZ = static_cast<double>(z1 - z2);
		return static_cast<float>(sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ));
	}
}